import {gUserInfo} from './info';
import image from './assets/images/avatardefault_92824.webp';
function App() {

  return (
    <div>
      <h5>{gUserInfo.firstname + " " + gUserInfo.lastname}</h5>
      <img src={image} width="300px" />
      <p>Tuổi của user là: {gUserInfo.age}</p>
      <p>{gUserInfo.age < 35 ? "Anh còn trẻ" : "Anh đã già"}</p>
      <p>Ngôn ngữ: </p>
      <ul>{gUserInfo.language.map(function(pElement, pIndex) {
            return <li>{pElement}</li>;
          })}
 
      </ul>
    </div>
  );
}

export default App;
